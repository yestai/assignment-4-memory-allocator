#include <stdio.h>
#include <stdlib.h>

#include "mem.h"

typedef void (*TestFunction)(void *);

void test(TestFunction test_function) {
    void *heap = heap_init(0);
    printf("Running test: ");
    test_function(heap);
    heap_term();
    printf("Success!\n");
}

void allocation_test(void *heap) {
    void *ptr1 = _malloc(50);
    void *ptr2 = malloc(50 * 4);
    debug_heap(stderr, heap);
    _free(ptr1);
    _free(ptr2);
}

void free_single_block_test(void *heap) {
    _malloc(30);
    void *ptr = _malloc(40);
    debug_heap(stderr, heap);
    _free(ptr);
}

void free_two_blocks_test(void *heap) {
    _malloc(30);
    void *ptr1 = _malloc(40);
    void *ptr2 = _malloc(40 * 4);
    debug_heap(stderr, heap);
    _free(ptr1);
    _free(ptr2);
}

void region_expansion_test(void *heap) {
    void *ptr1 = _malloc(4096);
    debug_heap(stderr, heap);
    void *ptr2 = _malloc(4096);
    debug_heap(stderr, heap);
    void *ptr3 = _malloc(4096);
    debug_heap(stderr, heap);
    _free(ptr1);
    _free(ptr2);
    _free(ptr3);
}

void new_region_test(void *heap) {
    void *ptr1 = mmap(heap + 4096, 4096, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, -1, 0);
    debug_heap(stderr, heap);
    void *ptr2 = _malloc(4096);
    debug_heap(stderr, heap);
    void *ptr3 = _malloc(4096);
    debug_heap(stderr, heap);
    _free(ptr2);
    _free(ptr3);
    _free(ptr1);
}

int main() {
    test(allocation_test);
    test(free_single_block_test);
    test(free_two_blocks_test);
    test(region_expansion_test);
    test(new_region_test);

    return 0;
}